//
//  ViewController.swift
//  DigitalClock
//
//  Created by Steven Mann on 2018-01-31.
//  Copyright © 2018 Steven Mann. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var clockColour: UISegmentedControl!
    @IBOutlet weak var backgroundColour: UISegmentedControl!
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.updateTimer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        
        settingsView.isHidden = true
        settingsButton.alpha = 0.25
        
        settingsView.layer.cornerRadius = 5
        settingsButton.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updateTimer(){
        
        let timeFormatter = DateFormatter()
        
        timeFormatter.timeStyle = .medium
        label.text = timeFormatter.string(from: Date())
    
    }
    
    @IBAction func settings(_ sender: Any) {
        
        if settingsView.isHidden == true{
            settingsView.isHidden = false
            settingsButton.setTitle("Hide Settings", for: UIControlState.normal)
            settingsButton.alpha = 1.0
        } else{
            settingsView.isHidden = true
            settingsButton.setTitle("Show Settings", for: UIControlState.normal)
            settingsButton.alpha = 0.25
        }
        
    }
    
    @IBAction func clockColourAction(_ sender: Any) {
        
        if clockColour.selectedSegmentIndex == 0{
            label.textColor = UIColor.white
        }
        else if clockColour.selectedSegmentIndex == 1{
            label.textColor = UIColor.black
        }
        else if clockColour.selectedSegmentIndex == 2{
            label.textColor = UIColor.red
        }
        else if clockColour.selectedSegmentIndex == 3{
            label.textColor = UIColor.green
        }
        
    }
    
    @IBAction func backgroundColourAction(_ sender: Any) {
        
        if backgroundColour.selectedSegmentIndex == 0{
            view.backgroundColor = UIColor.black
        }
        else if backgroundColour.selectedSegmentIndex == 1{
            view.backgroundColor = UIColor.white
        }
        else if backgroundColour.selectedSegmentIndex == 2{
            view.backgroundColor = UIColor.yellow
        }
        else if backgroundColour.selectedSegmentIndex == 3{
            view.backgroundColor = UIColor.blue
        }
        
        
    }

}

